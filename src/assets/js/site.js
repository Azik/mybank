$(window).on("load", function (e) {
	//EqualHeights
	eqHeight(".paket_list .box", 767);
});

$(function(){
	
	$(".lang span").click(function(){
		if($(this).parent().hasClass("active")){
			$(this).parent().stop(true, true).removeClass("active");
			$(this).next().stop(true, true).slideUp();
		} else {
			$(this).next().stop(true, true).slideDown();
			$(this).parent().stop(true, true).addClass("active");
		}
	});

	//Search
	$(".btn_search").click(function(){
		$(".search").stop(true, true).addClass("show");
	});
	
	$(document).click(function(e){
		if (!$(e.target).is(".search, .search *, .btn_search, .btn_search *")) {
			$(".search").stop(true, true).removeClass("show");
		}
	});
	
});

function eqHeight(param, sizes) {
	if ($(param).length){
		var resizeTimer;
		$( window ).resize(function() {
			var width = $(window).width();
			clearTimeout(resizeTimer);
			resizeTimer = setTimeout(func, 500);
		});
		func()
	}

	function func(){
		var width = $(window).width();
		$(param).removeAttr('style');
		if (width < sizes){
			$(param).height("auto");
		}else{
			$(param).equalHeights();
		}
	}
}