// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
export const REST_API_URL = 'https://jsonplaceholder.typicode.com/todos/1';
export const environment = {
  production: false,
  authKey: 'Authorization', 
  
   URLS: {
    GETRATES:  REST_API_URL + '/getrates',
//    CATEGORIES: REST_API_URL + '/categories/',
	CATEGORIES: REST_API_URL,
	RATES: REST_API_URL,
  },

  
};
