import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DebetCardsComponent } from './debet-cards.component';

describe('DebetCardsComponent', () => {
  let component: DebetCardsComponent;
  let fixture: ComponentFixture<DebetCardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DebetCardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DebetCardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
