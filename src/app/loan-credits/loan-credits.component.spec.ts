import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoanCreditsComponent } from './loan-credits.component';

describe('LoanCreditsComponent', () => {
  let component: LoanCreditsComponent;
  let fixture: ComponentFixture<LoanCreditsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoanCreditsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoanCreditsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
