import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';

@Injectable()
export class HttpClientService {

  constructor(private http: HttpClient) {}

  private createAuthorizationHeader(headers: HttpHeaders) {
    const lang = localStorage.getItem('lang');
    const accessToken = localStorage.getItem('access_token');

    if (lang) {
      headers = headers.append('Accept-Language', lang);
    } else {
      headers = headers.append('Accept-Language', 'az');
    }

    if (accessToken) {
      headers = headers.append(environment.authKey, 'bearer ' + accessToken);
    }

    headers.append('Accept', 'application/json');

    return headers;
  }

  public get(url, params?: HttpParams, isPublic?: boolean) {
    let headers = new HttpHeaders();
    if (!isPublic) {
      headers = this.createAuthorizationHeader(headers);
    }
    headers = headers.append('Content-Type', 'application/json');
    return this.http.get(url, {
      headers: headers,
      params: params
    });
  }

  public getHTML(url, params?: HttpParams) {
    let headers = new HttpHeaders();
    headers = this.createAuthorizationHeader(headers);
    headers = headers.append('Content-Type', 'application/json');
    return this.http.get(url, {
      headers: headers,
      params: params,
      responseType: 'text'
    });
  }

  public getEmptyResponse(url, params?: HttpParams) {
    let headers = new HttpHeaders();
    headers = this.createAuthorizationHeader(headers);
    headers = headers.append('Content-Type', 'application/json');
    return this.http.get(url, {
      headers: headers,
      params: params,
      responseType: 'text'
    });
  }

  public getEncoded(url, params?: HttpParams) {
    let headers = new HttpHeaders();
    headers = this.createAuthorizationHeader(headers);
    headers = headers.append('Content-Type', 'application/x-www-form-urlencoded');
    return this.http.get(url, {headers: headers, params: params});
  }

  public post(url, data?, params?) {
    let headers = new HttpHeaders();
    headers = this.createAuthorizationHeader(headers);
    headers = headers.append('Content-Type', 'application/json');
    return this.http.post(url, data, {
      headers: headers,
      params: params
    });
  }

  public async postAsync(url, data?, params?) {
    let headers = new HttpHeaders();
    headers = this.createAuthorizationHeader(headers);
    headers = headers.append('Content-Type', 'application/json');
    return await this.http.post(url, data, {
      headers: headers,
      params: params
    }).toPromise();
  }

  public postWithResponseType(url, data?, responseType?) {
    let headers = new HttpHeaders();
    headers = this.createAuthorizationHeader(headers);
    headers = headers.append('Content-Type', 'application/json');
    return this.http.post(url, data, {
      headers: headers,
      responseType: responseType
    });
  }

  public put(url, data?) {
    let headers = new HttpHeaders();
    headers = this.createAuthorizationHeader(headers);
    headers = headers.append('Content-Type', 'application/json');
    return this.http.put(url, data, {
      headers: headers
    });
  }

  public delete(url, data?, params?: HttpParams) {
    let headers = new HttpHeaders();
    headers = this.createAuthorizationHeader(headers);
    headers = headers.append('Content-Type', 'application/json');
    return this.http.request('delete', url, {
      headers: headers,
      body: data,
      params: params
    });
  }

  public postUrlEncoded(url, body) {
    let headers = new HttpHeaders();
    headers = this.createAuthorizationHeader(headers);
    headers = headers.append('Content-Type', 'text/plain');
    return this.http.post(url, body, {
      headers: headers
    });
  }


  public setAccessToken(accessToken: string) {
    localStorage.setItem('access_token', accessToken);
  }

  public setLanguage(lang: string) {
    localStorage.setItem('lang', lang);
  }

  public getParam(name: string) {
    return localStorage.getItem(name);
  }

  public getAccessToken() {
    return localStorage.getItem('access_token');
  }

  public clear() {
    localStorage.clear();
  }
}
