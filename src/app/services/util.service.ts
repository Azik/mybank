import {Injectable} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedRoute, NavigationExtras, Router} from '@angular/router';
import {StorageService} from './storage.service';
import {HttpClientService} from './http-client.service';
import {environment} from '../../environments/environment';
import {FormControl, FormGroup, Validators} from '@angular/forms';

declare var $: any;

@Injectable()
export class UtilService {

  sub: any;

  constructor( private translateService: TranslateService,
              private router: Router, private storageService: StorageService, private httpClientService: HttpClientService,
              private route: ActivatedRoute) { }


  goTo(path: string, data?: any, index?: number, id?: string) {
    if (path.indexOf('search') === -1) {
      $('#globalSearch').val('');
    }
    this.storageService.setData(data);
    this.storageService.setIndex(index);
    if (id) {
      this.router.navigate(['/' + path, id]);
    } else {
      this.router.navigate(['/' + path]);
    }

  }

  goToWithId(path: string, id: string) {
    if (path.indexOf('search') === -1) {
      $('#globalSearch').val('');
    }
    this.router.navigate(['/' + path, id]);
  }

  formatDate(dateTime: string) {
    let date = '';
    if (dateTime) {
      const aryDate = dateTime.split(' ');
      date = aryDate[0];
      const time = aryDate[1];
      date = date.substring(8, 10) + '/' + date.substring(5, 7) + '/' + date.substring(0, 4);
    }
    return date;
  }

  formatDateForAPI(date) {
    return date.substring(6, 10) + '/' + date.substring(3, 5) + '/' + date.substring(0, 2);
  }



}
