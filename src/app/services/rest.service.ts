import {Injectable} from '@angular/core';
import {URLSearchParams} from '@angular/http';
import {environment} from '../../environments/environment';
import {HttpClientService} from './http-client.service';
import {StorageService} from './storage.service';
import {UtilService} from './util.service';
import {HttpClient, HttpParams} from '@angular/common/http';
import { Component, OnInit } from '@angular/core';


@Injectable()
export class RestService {

  api: any;

  constructor(private http: HttpClient, private httpClientService: HttpClientService, 
               private utilService: UtilService,) { 
  }


  getRates() {
    return this.httpClientService.get(environment.URLS.RATES);
  }

}
