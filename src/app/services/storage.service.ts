import { Injectable } from '@angular/core';

@Injectable()
export class StorageService {

  private data: any;
  private index: number;  // multibilling payment index

  constructor() { }

  setData(data: any) {
    this.data = data;
  }

  getData() {
    return this.data;
  }

  setIndex(index: number) {
    this.index = index;
  }

  getIndex() {
    return this.index;
  }

}
