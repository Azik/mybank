import {AfterViewInit, Component} from '@angular/core';
import {NavigationEnd, NavigationStart, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
declare var $: any;
declare var ga: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {
  fullLayout = false;
  title = 'Angular7-unit-testing!';

  constructor(private router: Router, private translate: TranslateService){

    translate.setDefaultLang('ru');
  }

  ngAfterViewInit() {
    this.router.events.subscribe((path) => {
      if (path instanceof NavigationStart) {
      }
     /* if (path instanceof NavigationEnd) {
        this.checkIsLayout(path);

        checkIsLayout(path) {
          if (path.url.indexOf('/') > -1) {
            this.fullLayout = true;
          }
        }
      }
      */
    });
  }
}
