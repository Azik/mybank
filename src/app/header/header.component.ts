import { Component, OnInit } from '@angular/core';
import {HttpClientService} from '../services/http-client.service';
import {Rates} from '../models/rates';
import {StorageService} from '../services/storage.service';
import {UtilService} from '../services/util.service';
import {RestService} from '../services/rest.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  providers: [UtilService, RestService, HttpClientService, StorageService, RestService]
})
export class HeaderComponent implements OnInit {

  rates: [Rates];
  constructor(private restService: RestService) {
  	this.getRates();
   }

  ngOnInit() {
  }

  
  getRates() {

    this.restService.getRates()
    /* .catch((error: any) => {
        if (error.error) {
          this.errorMessage = error.error.userMessage;
        }
        return Observable.throw(error);
      }) */
      .subscribe(
      result => {
        debugger;
        const data: any = 		[{
          "name": "USD",
          "value": "1.64",
          "nominal": "AZN",
        },
        {
          "name": "EUR",
          "value": "1.60",
          "nominal": "AZN",
        },
        {
          "name": "RUR",
          "value": "0.10",
          "nominal": "AZN",
        },
        {
          "name": "GBP",
          "value": "0.10",
          "nominal": "AZN",
        },
        {
          "name": "TRY",
          "value": "0.10",
          "nominal": "AZN",
        },
      
      ]
    ;
        this.rates = data;
      });
  }


}
