import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';

import {RouterModule, Route} from '@angular/router';

// import ngx-translate and the http loader
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import { LoansComponent } from './loans/loans.component';
import { MortgageComponent } from './mortgage/mortgage.component';
import { LoanCreditsComponent } from './loan-credits/loan-credits.component';
import { DebetCardsComponent } from './debet-cards/debet-cards.component';

const routeConfig: Route[] = [
  {
    path: '',
    component: LandingPageComponent
  },
  {
    path: 'loans',
    component: LoansComponent
  },
  {
    path: 'mortgage',
    component: MortgageComponent
  },
  {
    path: 'loancredits',
    component: LoanCreditsComponent
  },
  {
    path: 'debetcards',
    component: DebetCardsComponent
  },
];

@NgModule({
  declarations: [
    AppComponent,
    LandingPageComponent,
    HeaderComponent,
    FooterComponent,
    LoansComponent,
    MortgageComponent,
    LoanCreditsComponent,
    DebetCardsComponent,
  ],
  imports: [

  // configure the imports
  HttpClientModule,
  TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
  }),

    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(routeConfig)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}