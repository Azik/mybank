export class Rates {

  Name: string;
  Nominal: string;
  Value: string;
  Arrow: false;

  constructor() {
    this.Name = '';
    this.Nominal = '';
    this.Value = '';
    this.Arrow = false;
  }

}
